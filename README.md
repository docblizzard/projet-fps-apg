# Projet FPS APG

Projet de création d'un jeux de type FPS DOOM ( Point de vue du personnage ) utilisant la bibliothèque de rendu fait maison et GL4Dummies.
## Description
Le but de ce projet était de créer un petit jeu similaire aux jeux FPS (vue à la première personne) comme l'ancien jeu Wolfenstein ou DOOM, je suis parti sur le code fait en cours qui permettait de déplacer un bloc. J'ai modifié tout d'abord la vue de la caméra pour que celle-ci suive le personnage dans une vue à la première personne, c'est à dire ce que le personnage verrat directement. Le but était de faire une rotation de la caméra lorsque l'on appuie sur les touches de gauche (q) ou droite (d), malheureusement je ne trouvais pas comment tourner la scène ( le groupe de cube réprésentant le contour d'une salle), enfin la scène tournait mais seulement en son milieu, donc si je bougeais le personnage la scène continuerait de tourner sur son milieu et non en prenant le milieu la position du personnage. La création de la balle se ferait en cliquant sur le bouton gauche de la souris, une balle se projeterait en partant de la position du personnage et continuerait sur son axe Z jusqu'à un certain montant puis la position de la balle reviendrait à son point de départ jusqu'à que l'on re-clique sur la souris. C'est le meilleur moyen que j'avais trouvé car je n'ai pas compris comment supprimer un objet et le re-créer sans complication. Malheureusement l'implémentation de cette fonction n'est pas fonctionel aussi, la balle part sans problème et revient, mais elle reste sur la position principale, et ne suit pas la position du personnage quand elle va se lancer, mais quand le personnage se déplace sur l'axe Z, là la balle part bien là ou le personnage se trouve. De même la balle suivrat le personnage n'importe ou tant qu'il n'a pas tiré, mais une fois qu'il tire, la balle revient sur la position de départ et effectue son lancé.

## Roadmap
J'aurais bien sûr aimé implémenté des "ennemis" qui se déplaceraient à l'intérieur de la scène et pourraient "mourir" si ils se faisaient tirer dessus, mais honnètement je n'aurais aucune idée de comment implémenter ça, j'imagine en comparant la positon x et z de la balle et de "l'ennemi" et en prenant en compte la taille des cubes.

## Project status

https://gitlab.com/docblizzard/projet-fps-apg
