/*!\file window.c
 * \brief Utilisation du raster DIY comme pipeline de rendu 3D. Cet
 * exemple montre l'affichage d'une grille de cubes.
 * \author FarÃ¨s BELHADJ, amsi@up8.edu
 * \date December 02, 2021.
 */
#include <assert.h>
 /* inclusion des entÃªtes de fonctions de gestion de primitives simples
  * de dessin. La lettre p signifie aussi bien primitive que
  * pÃ©dagogique. */
#include <GL4D/gl4dp.h>
  /* inclure la bibliothÃ¨que de rendu DIY */
#include "rasterize.h"

/* inclusion des entÃªtes de fonctions de crÃ©ation et de gestion de
 * fenÃªtres systÃ¨me ouvrant un contexte favorable Ã  GL4dummies. Cette
 * partie est dÃ©pendante de la bibliothÃ¨que SDL2 */
#include <GL4D/gl4duw_SDL2.h>

 /* protos de fonctions locales (static) */
static void init(void);
static void idle(void);
static void draw(void);
static void draw2(void);
static void mouse(int button, int state, int x, int y);
static void keyd(int keycode);
static void keyu(int keycode);
static void sortie(void);

/* Création des surfaces des murs, du personnage et des ennemis*/
static surface_t* _cube = NULL;
static surface_t* balle = NULL;
static surface_t* ennemi = NULL;

static float _cubeSize = 4.0f;

/* des variable d'Ã©tats pour activer/dÃ©sactiver des options de rendu */
static int _use_tex = 1, _use_color = 1, _use_lighting = 1;


/* on crÃ©Ã© une grille de positions oÃ¹ il y aura des cubes */
static int _grille[] = {
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
  1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
  1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
  1, 0, 0, 1, 0, 0, 0, 0, 0, 1,
  1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
  1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
  1, 1, 0, 0, 0, 0, 0, 0, 0, 1,
  1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
  1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
};
static int _grilleW = 10;
static int _grilleH = 10;

typedef struct perso_t perso_t;
struct perso_t {
    float x, y, z;
};

perso_t _hero = { 0.0f, 0.0f, 0.0f };
perso_t mov = { 0.0f, 0.0f, 0.0f };

enum {
    RIGHT = 0,
    UP,
    LEFT,
    DOWN,
    /* toujours Ã  la fin */
    VK_SIZEOF
};
enum {
    LEFT_CLICK = 0,
    VM_SIZEOF
};

/* clavier virtuel */

int _vkeyboard[VK_SIZEOF] = { 0, 0, 0, 0 };
int vmouse[VM_SIZEOF] = { 0 };
/*!\brief paramÃ¨tre l'application et lance la boucle infinie. */
int main(int argc, char** argv) {
    /* tentative de crÃ©ation d'une fenÃªtre pour GL4Dummies */
    if (!gl4duwCreateWindow(argc, argv, /* args du programme */
        "Lidl DOOM", /* titre */
        10, 10, 800, 600, /* x, y, largeur, heuteur */
        GL4DW_SHOWN)){
   
        return 1;
    }
    init();
    gl4duwMouseFunc(mouse);
    /* mettre en place la fonction d'interception clavier touche pressÃ©e */
    gl4duwKeyDownFunc(keyd);
    /* mettre en place la fonction d'interception clavier touche relachÃ©e */
    gl4duwKeyUpFunc(keyu);
    /* mettre en place la fonction idle (simulation, au sens physique du terme) */
    gl4duwIdleFunc(idle);
    /* mettre en place la fonction de display */
    gl4duwDisplayFunc(draw);
    /* boucle infinie pour Ã©viter que le programme ne s'arrÃªte et ferme
     * la fenÃªtre immÃ©diatement */
    gl4duwMainLoop();
    gl4duSendMatrices();
    return 0;
}

/*!\brief init de nos donnÃ©es, spÃ©cialement les trois surfaces
 * utilisÃ©es dans ce code */
void init(void) {
    GLuint id;
    vec4 r = { 1, 0, 0, 1 }, g = { 0, 1, 0, 1 }, b = { 0, 0, 1, 1 };
    /* crÃ©ation d'un screen GL4Dummies (texture dans laquelle nous
     * pouvons dessiner) aux dimensions de la fenÃªtre.  IMPORTANT de
     * crÃ©er le screen avant d'utiliser les fonctions liÃ©es au
     * textures */
    gl4dpInitScreen();
    /* Pour forcer la dÃ©sactivation de la synchronisation verticale */
    SDL_GL_SetSwapInterval(1);
    /* on crÃ©Ã© le cube */
    _cube = mk_cube();         /* Ã§a fait 2x6 triangles      */
    balle = mk_sphere(6,6);
    balle->dcolor = g;
    /* on change la couleur */
    _cube->dcolor = b;
    /* on leur rajoute la texture */
    id = get_texture_from_BMP("images/tex.bmp");
    set_texture_id(_cube, id);
    set_texture_id(balle, id);
    /* si _use_tex != 0, on active l'utilisation de la texture */
    if (_use_tex) {
        enable_surface_option(_cube, SO_USE_TEXTURE);
        enable_surface_option(balle, SO_USE_TEXTURE);
    }
    /* si _use_lighting != 0, on active l'ombrage */
    if (_use_lighting) {     
        enable_surface_option(_cube, SO_USE_LIGHTING);
        enable_surface_option(balle, SO_USE_LIGHTING);
    }
    /* mettre en place la fonction Ã  appeler en cas de sortie */
    atexit(sortie);
}

/*!\brief la fonction appelÃ©e Ã  chaque idle. */
void idle(void) {
    /* on va rÃ©cupÃ©rer le delta-temps */
    static double t0 = 0.0; // le temps Ã  la frame prÃ©cÃ©dente
    double t, dt;
    t = gl4dGetElapsedTime();
    dt = (t - t0) / 1000.0; // diviser par mille pour avoir des secondes
    // pour le frame d'aprÃ¨s, je mets Ã  jour t0
    t0 = t;

   /* if (_vkeyboard[RIGHT])
        _hero.x += 12.0f * dt; */
    if (_vkeyboard[UP])
        _hero.z -= 12.0f * dt;
    /*if (_vkeyboard[LEFT])
        _hero.x -= 12.0f * dt; */
    if (_vkeyboard[DOWN])
        _hero.z += 12.0f * dt;

    int li, col;
    col = (int)((_hero.x + _cubeSize * _grilleW / 2) / _cubeSize);
    li = (int)((_hero.z + _cubeSize * _grilleH / 2) / _cubeSize);
    printf("col = %d, li = %d\n", col, li);
}


/*!\brief la fonction appelÃ©e Ã  chaque display. */
void draw(void) {
    vec4 r = { 1, 0, 0, 1 }, b = { 0, 0, 1, 1 }, g = { 0, 1, 0, 1 };
    /* on va rÃ©cupÃ©rer le delta-temps */
    static double t0 = 0.0; // le temps Ã  la frame prÃ©cÃ©dente
    double t, dt;
    t = gl4dGetElapsedTime();
    dt = (t - t0) / 1000.0; // diviser par mille pour avoir des secondes
    // pour le frame d'aprÃ¨s, je mets Ã  jour t0
    t0 = t;
    /* fin de rÃ©cupÃ©ration de delta-temps */
    static float a = 0.0f;
    float model_view_matrix[16], projection_matrix[16], nmv[16];
    /* effacer l'Ã©cran et le buffer de profondeur */
    gl4dpClearScreen();
    clear_depth_map();
    /* des macros facilitant le travail avec des matrices et des
     * vecteurs se trouvent dans la bibliothÃ¨que GL4Dummies, dans le
     * fichier gl4dm.h */
     /* charger un frustum dans projection_matrix */
    MFRUSTUM(projection_matrix, -0.05f, 0.05f, -0.05f, 0.05f, 0.1f, 1000.0f);
    /* charger la matrice identitÃ© dans model-view */
    MIDENTITY(model_view_matrix);
    /* Good FPS view camera */
    lookAt(model_view_matrix, _hero.x , _hero.y + 3, _hero.z+5, _hero.x, _hero.y + 2, _hero.z -10, 0, 1, 0);


    /* pour centrer la grille par rapport au monde */
    float cX = -_cubeSize * _grilleW / 2.0f;
    float cZ = -_cubeSize * _grilleH / 2.0f;
    /* on change la couleur */
    _cube->dcolor = b;
    /* pour toutes les cases de la grille, afficher un cube quand il y a
     * un 1 dans la grille */
    for (int i = 0; i < _grilleW; ++i) {
        for (int j = 0; j < _grilleH; ++j) {
            if (_grille[i * _grilleW + j] == 1) {
                /* copie model_view_matrix dans nmv */
                memcpy(nmv, model_view_matrix, sizeof nmv);
                /* pour tourner tout le plateau */
                if (_vkeyboard[RIGHT]) {
                    translate(nmv, _hero.x, _hero.y, _hero.z);
                    rotate(nmv, a, 0.0f, 1.0f, 0.0f);
                }
                if (_vkeyboard[LEFT]) {
                    translate(nmv, _hero.x, _hero.y, _hero.z);
                    rotate(nmv, a, 0.0f, -1.0f, 0.0f);
                }

                //rotate(nmv, a, 0.0f, 1.0f, 0.0f);
                /* pour convertir les coordonnÃ©es i,j de la grille en x,z du monde */
                translate(nmv, _cubeSize * j + cX, 0.0f, _cubeSize * i + cZ);
                scale(nmv, _cubeSize / 2.0f, _cubeSize / 2.0f, _cubeSize / 2.0f);
                transform_n_rasterize(_cube, nmv, projection_matrix);
            }
        }
    }
    // Creation de la balle qui part du hero et parcours une distance fixe avant de revenir à son point
     //balle->dcolor = g;
     //memcpy(nmv, model_view_matrix, sizeof nmv);
    if (!vmouse[VM_SIZEOF]) {
        mov.x = _hero.x;
        mov.y = _hero.y;
        mov.z = _hero.z;
    }
    if (vmouse[VM_SIZEOF]) {
        

        balle->dcolor = g;
        memcpy(nmv, model_view_matrix, sizeof nmv);

        if (mov.z > (_hero.z - 25) && vmouse[VM_SIZEOF] == 1) {
            mov.z -= 2;
        }
        if (mov.z <= _hero.z - 25 && vmouse[VM_SIZEOF] == 1) {
            mov.z = _hero.z;
            vmouse[VM_SIZEOF] = 0;
        }
        translate(nmv, mov.x, mov.y, mov.z - 2);
        scale(nmv, _cubeSize / 2.0f, _cubeSize / 2.0f, _cubeSize / 2.0f);
        transform_n_rasterize(balle, nmv, projection_matrix);
    }
    
    
    
    /* on dessine le perso _hero */
    _cube->dcolor = r;
    memcpy(nmv, model_view_matrix, sizeof nmv);
    translate(nmv, _hero.x, _hero.y, _hero.z);
    rotate(nmv, a, 0.0f, 1.0f, 0.0f);
    scale(nmv, _cubeSize / 2.0f, _cubeSize / 2.0f, _cubeSize / 2.0f);
    transform_n_rasterize(_cube, nmv, projection_matrix);

    /* dÃ©clarer qu'on a changÃ© des pixels du screen (en bas niveau) */
    gl4dpScreenHasChanged();
    /* fonction permettant de raffraÃ®chir l'ensemble de la fenÃªtre*/
    gl4dpUpdateScreen(NULL);
    a += 0.1f * 360.0f * dt;
}
// Intercepte l'évenement souris pour modifier les options
static void mouse(int button, int state, int x, int y) {
    if (state && button == GL4D_BUTTON_LEFT) {
        vmouse[VM_SIZEOF] = 1;
     }
}
/*!\brief intercepte l'Ã©vÃ©nement clavier pour modifier les options. */
void keyd(int keycode) {
    switch (keycode) {
    case GL4DK_t: /* 't' la texture */
        _use_tex = !_use_tex;
        if (_use_tex) {
            enable_surface_option(_cube, SO_USE_TEXTURE);
        }
        else {
            disable_surface_option(_cube, SO_USE_TEXTURE);
        }
        break;
    case GL4DK_c: /* 'c' utiliser la couleur */
        _use_color = !_use_color;
        if (_use_color) {
            enable_surface_option(_cube, SO_USE_COLOR);
        }
        else {
            disable_surface_option(_cube, SO_USE_COLOR);
        }
        break;
    case GL4DK_l: /* 'l' utiliser l'ombrage par la mÃ©thode Gouraud */
        _use_lighting = !_use_lighting;
        if (_use_lighting) {
            enable_surface_option(_cube, SO_USE_LIGHTING);
        }
        else {
            disable_surface_option(_cube, SO_USE_LIGHTING);
        }
        break;
    
    case GL4DK_d:
        _vkeyboard[RIGHT] = 1;
        break;
    case GL4DK_z:
        _vkeyboard[UP] = 1;
        break;
    case GL4DK_q:
        _vkeyboard[LEFT] = 1;
        break;
    case GL4DK_s:
        _vkeyboard[DOWN] = 1;
        break;
    default: break;
    }
}

/*!\brief intercepte l'Ã©vÃ©nement clavier pour modifier les options. */
void keyu(int keycode) {
    switch (keycode) {
    case GL4DK_d:
        _vkeyboard[RIGHT] = 0;
        break;
    case GL4DK_z:
        _vkeyboard[UP] = 0;
        break;
    case GL4DK_q:
        _vkeyboard[LEFT] = 0;
        break;
    case GL4DK_s:
        _vkeyboard[DOWN] = 0;
        break;
    default: break;
    }
}

/*!\brief Ã  appeler Ã  la sortie du programme. */
void sortie(void) {
    /* on libÃ¨re le cube */
    if (_cube) {
        free_surface(_cube);
        _cube = NULL;
    }
    if (balle) {
        free_surface(balle);
        balle = NULL;
    }
    /* libÃ¨re tous les objets produits par GL4Dummies, ici
     * principalement les screen */
    gl4duClean(GL4DU_ALL);
}